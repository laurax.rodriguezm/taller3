package com.example.taller3.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.taller3.R
import kotlinx.android.synthetic.main.dialog_product.*

class ProductDialog(context: Context, val name: String, val description: String, var stock: Int, private val callback: (String, String, Int) -> Unit) : Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_product)
        nameEditText.setText(name)
        descriptionEditText.setText(description)
        stockEditText.setText(""+stock)

        saveButton.setOnClickListener {
            makeValidation()

        }
    }


    private fun makeValidation(){

        if (nameEditText.text.isEmpty()|| descriptionEditText.text.isEmpty()|| stockEditText.text.isEmpty()){
            val builder= AlertDialog.Builder(context)
            builder.setMessage("Todos los campos son obligatorios")
            builder.setPositiveButton("Aceptar"){ _,_ ->

            }
            builder.show()
        }else {
            val name = nameEditText.text.toString()
            val description = descriptionEditText.text.toString()
            val stock= stockEditText.text.toString().toInt()
            callback(name, description, stock)
            dismiss()

        }

    }
}
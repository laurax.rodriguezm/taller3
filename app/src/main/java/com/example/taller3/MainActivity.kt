package com.example.taller3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.taller3.adapter.ProductAdapter
import com.example.taller3.dialogs.ProductDialog
import com.example.taller3.model.Product
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupButtons()
        setupList()
    }

    private fun setupButtons() {
        addButton.setOnClickListener {
            val dialog = ProductDialog(this, "", "", stock = 0) { name, description, stock ->
                addProduct(name, description, stock)
            }
            dialog.show()
        }
    }

    private fun setupList() {
        val products = mutableListOf(
            Product("Frijoles", "Bolsas de 1 Libra", 200),
            Product("Arroz", "Bolsas de 1 Libra", 800))

        adapter = ProductAdapter(products) { item, isDelete ->
            if(isDelete) deleteProduct(item)
            else editProduct(item)
        }
        productRecyclerView.adapter = adapter
        productRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addProduct(name: String, description: String, stock: Int) {
        val product = Product(name, description, stock)
        adapter.addProduct(product)
        adapter.notifyDataSetChanged()
    }

    private fun deleteProduct(product: Product) {

        val builder= AlertDialog.Builder(this)
        builder.setMessage("Seguro desea eliminar el Producto")
        builder.setPositiveButton("Aceptar"){ _,_ ->
            adapter.deleteProduct(product)
            adapter.notifyDataSetChanged()

        }
        builder.setNegativeButton("Cancelar") { dialog, position ->
        }
        builder.show()

    }

    private fun updateProduct(product: Product, name: String, description: String, stock: Int) {
        adapter.editProduct(product, name, description, stock)
        adapter.notifyDataSetChanged()
    }

    private fun editProduct(product: Product) {
        val dialog = ProductDialog(this, product.name, product.description, product.stock) { name, description, stock ->
            updateProduct(product, name, description, stock)
        }
        dialog.show()
    }
}